# Opensubs (name subject to change)

![Discord Server](https://img.shields.io/discord/987039798076252201?style=flat-square)

## Important

Note that the project is still in early development. It is not yet recommended that you attempt to create a dedicated server for it. If you wish to play, you should can host the client and server on your personal computer whilst you are playing. Bugs are to be expected, please check the [Taskade board](https://www.taskade.com/d/tmCV7XxY198XtMjD?share=edit&edit=shmZrNhuy1tpKUff) to see if they have been noted

## About 
This project is an attempt to remake the gameplay experience of the mobile strategy game [Subterfuge](http://subterfuge-game.com/) in the [Godot game engine](https://godotengine.org/). This project consists of two parts, the client (this repository) and the [server](https://codeberg.org/mossy/opensubs-server). This project is community-run and is not associated with the developers of the original game

## Contributing

If you wish to contribute, please do your best to stick to the style of code the project uses. This mainly consists of:

* Having statically typed variables
* Defining the return type of functions
* Functions should be named in `underscore_separated_lower_case`
* Function parameters should be named in `conjoinedlowercase`
* **Everything else** should be written in `UpperCamelCase`

The project has a [Taskade board](https://www.taskade.com/d/tmCV7XxY198XtMjD?share=edit&edit=shmZrNhuy1tpKUff) for organizing work that needs to be done

To start contributing:

1. Download and install the standard edition of Godot for your operating system [from the Godot website.](https://godotengine.org/download)
2. Fork the repository.
3. Create an SSH key and add it to your Codeberg account to be able to clone the repository. [See the instructions here.](https://docs.codeberg.org/security/ssh-key/)
4. Start Godot and import the project(s) once you have cloned them.

## Server operation

Run the server by running the server executable (this varies depending on your OS).

The server accepts the following command line options:

| Option Name | Description | Default |
| --- | --- | --- |
| --config | The path to the configuration file for the server. | ./ServerConfig.cfg |

Important Note: Command line options MUST be passed with an `=` as follows: `--key=value`. You cannot pass them as `--key value`. 

### Config File

The server will attempt to load configurations from a config file. It contains some connection and performance options you may want to change.
Make sure that you set the port number to one that is open on your network. The config file is an INI file format and accepts the following values for configuration:

| Option Name | Description | Default |
| --- | --- | --- |
| Connection.Port | The port that the server will be hosted on. | 25565 |
| Connection.MaxConnectedClients | The maximum number of clients that can connect to the server at once. | 100 |
| GameSetup.MaxPlayers | The maximum number of players that a client can request in their game lobby. | 10 |
| GameSetup.TickLengths | An array of values indicating the number of seconds per tick that a user may select when they create a lobby. | [1, 10, 60, 600] |

If the config file is not present when the server starts, a file will be created at the default location with the default values.
