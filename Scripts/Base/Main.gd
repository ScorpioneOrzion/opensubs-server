extends Node2D

onready var TickTimer : PackedScene = preload("res://Scenes/Timers/TickTimer.tscn")

func unpause(gameid : int) -> void:
	var TickLength : int = Network.Games[gameid]["TickLength"]
	var TickTimerNode : Node = get_node(str(gameid))
	TickTimerNode.start(TickLength)
	Network.unpause(gameid)

func create_tick_timer(gameid : int) -> void:
	var TickTimerInstance : Node = TickTimer.instance()
	TickTimerInstance.GameId = gameid
	TickTimerInstance.name = str(gameid)
	add_child(TickTimerInstance)
