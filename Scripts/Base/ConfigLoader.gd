extends Node

class_name ConfigLoader

# Default config values variables

var Port : int = 25565
var MaxConnectedClients : int = 100
var MaxPlayers : int = 10
var TickLengths : Array = [1, 10, 60, 600]

# Config file variables

var Config := ConfigFile.new()
var CliOptions: Dictionary = parse_cli_options()

# Loads the config file
func load_config() -> void:
	var ConfigFilePath : String = get_save_path()
	if Config.load(ConfigFilePath) != OK:
		print("Unable to load config file from: " + ConfigFilePath + "\nCreating a new default config file")
		save_config_file(ConfigFilePath)
	parse_config_file()

# Returns a dictionary of valid command line arguments
func parse_cli_options():
	var CommandLineOptions : Dictionary = {}
	for argument in OS.get_cmdline_args():
		if argument.find("=") > -1:
			var key_value = argument.split("=")
			CommandLineOptions[key_value[0].lstrip("--")] = key_value[1]
	print("CLI Arguments: " + str(CommandLineOptions))
	return CommandLineOptions
	
# Loads the config file
func parse_config_file() -> void:
	Port = Config.get_value("Connection", "Port", Port)
	MaxConnectedClients = Config.get_value("Connection", "MaxConnectedClients", MaxConnectedClients)
	MaxPlayers = Config.get_value("GameSetup", "MaxPlayers", MaxPlayers)
	TickLengths = Config.get_value("GameSetup", "TickLengths", TickLengths)

# Saves the config file to the given path
func save_config_file(path : String) -> void:
	Config.set_value("Connection", "Port", Port)
	Config.set_value("Connection", "MaxConnectedClients", MaxConnectedClients)
	Config.set_value("GameSetup", "MaxPlayers", MaxPlayers)
	Config.set_value("GameSetup", "TickLengths", TickLengths)
	if Config.save(path) != OK:
		print("Unable to save configuration file in specified location")
	
# Returns the location of the save file
func get_save_path() -> String:
	if CliOptions.has("config"):
		return CliOptions.get("config")
	else:
		return OS.get_executable_path().get_base_dir().plus_file("Server.save")
