extends Node

onready var Main : Node = get_node("/root/Main")

# Config file variables

var ConfigLoader = load("res://Scripts/Base/ConfigLoader.gd").new()

# Game variables

var Games : Dictionary  = {}
var Colors : Array = [
	"Dormant",
	"Red",
	"Orange",
	"Cyan",
	"Sky",
	"Blue",
	"Purple",
	"Pink",
	"Olive",
	"Beige",
	"Brown"
	]

# Inbuilt functions

func _ready() -> void:
	randomize()
	ConfigLoader.load_config()
	print("Loaded config file")
	var NetworkENet := NetworkedMultiplayerENet.new()
	NetworkENet.create_server(ConfigLoader.Port, ConfigLoader.MaxConnectedClients)
	print("Listening on port " + str(ConfigLoader.Port))
	get_tree().set_network_peer(NetworkENet)
	NetworkENet.connect("peer_connected", self, "peer_connected")
	NetworkENet.connect("peer_disconnected", self, "peer_disconnected")

# Connection functions

# Called when peer connects to server
func peer_connected(id : int) -> void:
	print(str(id) + " connected")

# Called when peer disconnects from server
func peer_disconnected(id) -> void:
	print(str(id) + " disconnected")
	disconnect_player(id)

# Returns all online players
func get_online_players(gameid : int) -> Array:
	var OnlinePlayers = Games[gameid]["OnlinePlayers"]
	return OnlinePlayers.keys()

# Returns the IDs of all online players in a game
func get_online_player_ids(gameid : int) -> Array:
	var OnlinePlayers = Games[gameid]["OnlinePlayers"]
	var Ids : Array = []
	for player in OnlinePlayers:
		Ids.append_array(OnlinePlayers[player])
	return Ids

# Save functions

# Returns the location of the save file
func get_save_path() -> String:
	return OS.get_executable_path().get_base_dir().plus_file("Server.save")

# Loads the save file
func load_save_file() -> void:
	var SaveFile = File.new()
	SaveFile.open(get_save_path(), File.WRITE)

# Tick functions

# Increases the global tick by 1
func increase_tick(gameid : int) -> void:
	var GameData : Dictionary = Games[gameid]
	GameData["GlobalTick"] += 1
	for playerid in get_online_player_ids(gameid):
		rpc_id(playerid, "set_global_tick", GameData["GlobalTick"])

# Remote functions

# Creates submarine
remote func create_submarine(gameid : int, id : int, tick : int, initialoutpost : String, username : String, initialtarget : String) -> void:
	var GameData : Dictionary = Games[gameid]
	var LaunchTime : int = -1
	if GameData["StartTime"] != -1:
		LaunchTime = OS.get_unix_time()
	var Order = {
		"SubmarineId" : randi(),
		"LaunchTime" : LaunchTime,
		"Type" : "Launch",
		"Tick" : tick,
		"InitialPlayer" : get_username_player(gameid, username),
		"InitialOutpost" : initialoutpost,
		"InitialTarget" : initialtarget,
		"InitialTroops" : 0,
	}
	GameData["Orders"].append(Order)
	rpc_id(id, "focus_next_submarine")
	for playerid in get_online_player_ids(gameid):
		rpc_id(playerid, "calculate_order", Order)

# Removes submarine
remote func cancel_submarine(gameid : int, submarineid : int) -> void:
	var GameData : Dictionary = Games[gameid]
	var SubmarineFound : bool = false
	for i in len(GameData["Orders"]):
		if GameData["Orders"][i]["Type"] == "Launch":
			if GameData["Orders"][i]["SubmarineId"] == submarineid:
				GameData["Orders"].remove(i)
				SubmarineFound = true
				break
	if SubmarineFound:
		cancel_gift_submarine(gameid, submarineid)
		for playerid in get_online_player_ids(gameid):
			rpc_id(playerid, "cancel_submarine", submarineid)

# Changes submarine troops whilst preparing
remote func prepare_submarine_troops(gameid : int, submarineid : int, troops : int) -> void:
	var Order : Dictionary = get_submarine_order(gameid, submarineid)
	Order["InitialTroops"] = troops
	for playerid in get_online_player_ids(gameid):
		rpc_id(playerid, "prepare_submarine_troops", submarineid, troops)

# Sets submarine gift and calculates history
remote func gift_submarine(gameid : int, tick : int, submarineid : int) -> void:
	var GameData : Dictionary = Games[gameid]
	var GiftTime : int = -1
	if GameData["StartTime"] != -1:
		GiftTime = OS.get_unix_time()
	var Order = {
		"SubmarineId" : submarineid,
		"GiftTime" : GiftTime,
		"Type" : "Gift",
		"Tick" : tick
	}
	GameData["Orders"].append(Order)
	for playerid in get_online_player_ids(gameid):
		rpc_id(playerid, "calculate_order", Order)

# Removes gift
remote func cancel_gift_submarine(gameid : int, submarineid : int) -> void:
	var GameData : Dictionary = Games[gameid]
	var SubmarineFound : bool = false
	for i in len(GameData["Orders"]):
		if GameData["Orders"][i]["Type"] == "Gift":
			if GameData["Orders"][i]["SubmarineId"] == submarineid:
				GameData["Orders"].remove(i)
				SubmarineFound = true
				break
	if SubmarineFound:
		for playerid in get_online_player_ids(gameid):
			rpc_id(playerid, "cancel_gift_submarine", submarineid)

# Game functions

# Creates a new game
remote func create_game(
	id : int,
	gamename : String,
	maxplayers : int,
	ticklength : int,
	launchwaitmodifier : int,
	giftwaitmodifier : int,
	username : String
) -> void:
	var Players : Dictionary = {"Dormant" : {"Username" : "Dormant", "Occupied" : true}}
	var AvailableColors : Array = Colors.duplicate(true)
	AvailableColors.erase("Dormant")
	for i in maxplayers:
		var PlayerColor : String = AvailableColors[rand_range(0, len(AvailableColors))]
		Players[PlayerColor] = {"Username" : "Empty", "Occupied" : false}
		AvailableColors.erase(PlayerColor)
	var NewGame = {
		"GameName" : gamename,
		"StartTime" : -1,
		"OnlinePlayers" : {},
		"MaxPlayers" : maxplayers,
		"GlobalTick" : 0,
		"VisibleTickMax" : 300,
		"Players" : Players,
		"OutpostData" : [],
		"Orders" : [],
		"TickLength" : ticklength,
		"SubmarineSpeed" : 30,
		"LaunchWaitModifier" : launchwaitmodifier,
		"GiftWaitModifier" : giftwaitmodifier,
		"OutpostStartingTroops" : 40,
		"GenerateTroopTicks" : 20,
		"GenerateShieldTicks" : 20,
		"GenerateTroopNumber" : 6,
		"GenerateShieldNumber" : 1,
		"Chats" : {}
	}
	var GameId : int = randi()
	Games[GameId] = NewGame
	join_game(GameId, id, username)
	var SaveFile = File.new()
	SaveFile.open(get_save_path(), File.WRITE)
	Main.create_tick_timer(GameId)

# Sends client current game state
remote func open_game(gameid : int, id : int, username : String) -> void:
	var GameData : Dictionary = Games[gameid]
	var UsernameColor : String = get_username_player(gameid, username)
	if GameData["OnlinePlayers"].has(UsernameColor) == false:
		GameData["OnlinePlayers"][UsernameColor] = []
	GameData["OnlinePlayers"][UsernameColor].append(id)
	var Chats : Dictionary
	for chat in GameData["Chats"]:
		if chat.has(UsernameColor):
			Chats[chat] = GameData["Chats"][chat]
	var OnlinePlayers : Array
	for player in GameData["OnlinePlayers"]:
		OnlinePlayers.append(player)
	var GameStateVariables : Array = [
		["StartTime", GameData["StartTime"]],
		["OnlinePlayers", OnlinePlayers],
		["GlobalTick", GameData["GlobalTick"]],
		["TickLength", GameData["TickLength"]],
		["SubmarineSpeed", GameData["SubmarineSpeed"]],
		["LaunchWaitModifier", GameData["LaunchWaitModifier"]],
		["GiftWaitModifier", GameData["GiftWaitModifier"]],
		["OutpostStartingTroops", GameData["OutpostStartingTroops"]],
		["GenerateShieldTicks", GameData["GenerateShieldTicks"]],
		["GenerateTroopTicks", GameData["GenerateTroopTicks"]],
		["GenerateTroopNumber", GameData["GenerateTroopNumber"]],
		["GenerateShieldNumber", GameData["GenerateShieldNumber"]],
		["Players", GameData["Players"]],
		["OnlinePlayers", OnlinePlayers],
		["Chats", Chats]
	]
	print("Sending game variables")
	for variable in GameStateVariables:
		rpc_id(id, "set_game_variable", variable[0], variable[1])
	rpc_id(id, "calculate_game_state", GameData["Orders"])
	for playerid in get_online_player_ids(gameid):
		if playerid != id:
			rpc_id(playerid, "add_online_player", UsernameColor)
	print("Sending game state updated call")

# Return basic information about a game
func get_basic_game_info(gameid : int) -> Dictionary:
	var GameData : Dictionary = Games[gameid]
	var OccupiedPlayers : Array = []
	for player in GameData["Players"]:
		if GameData["Players"][player]["Occupied"] == true and GameData["Players"][player]["Username"] != "Dormant":
			OccupiedPlayers.append(player)
	return {"GameName" : GameData["GameName"], "OccupiedPlayers" : len(OccupiedPlayers), "MaxPlayers" : GameData["MaxPlayers"]}

# Returns the games a username is present in
remote func get_joined_games(id : int, username : String) -> void:
	var GamesInfo : Dictionary = {}
	for gameid in Games:
		if get_username_in_game(gameid, username) == true:
			GamesInfo[gameid] = get_basic_game_info(gameid)
	rpc_id(id, "set_joined_games", GamesInfo)

# Returns the games a username is not present in
remote func get_open_games(id : int, username : String) -> void:
	var GamesInfo : Dictionary = {}
	for gameid in Games:
		if get_username_in_game(gameid, username) == false:
			GamesInfo[gameid] = get_basic_game_info(gameid)
	rpc_id(id, "set_open_games", GamesInfo)

# Adds a player to a game
func add_joined_game(gameid : int, id : int) -> void:
	rpc_id(id, "add_joined_game", gameid, get_basic_game_info(gameid))

# Checks if a player can join a game and adds them to it
remote func join_game(gameid : int, id : int, username : String) -> void:
	var GameData : Dictionary = Games[gameid]
	var Players : Dictionary = GameData["Players"]
	var UnoccupiedPlayers : Array = []
	for player in Players:
		if Players[player]["Occupied"] == false:
			UnoccupiedPlayers.append(player)
	if UnoccupiedPlayers.empty() == false:
		if check_username(gameid, username):
			var UnoccupiedPlayer : String = UnoccupiedPlayers[rand_range(0, len(UnoccupiedPlayers))]
			var Player : Dictionary = Players[UnoccupiedPlayer]
			Player["Occupied"] = true
			Player["Username"] = username
			for playerid in get_online_player_ids(gameid):
				if playerid != id:
					rpc_id(playerid, "set_player", UnoccupiedPlayer, Player)
			if len(UnoccupiedPlayers) - 1 == 0:
				Main.unpause(gameid)
			add_joined_game(gameid, id)

# Unpauses the game
func unpause(gameid : int) -> void:
	var GameData : Dictionary = Games[gameid]
	GameData["StartTime"] = OS.get_unix_time()
	for playerid in get_online_player_ids(gameid):
		rpc_id(playerid, "set_game_variable", "StartTime", OS.get_unix_time())

# Sends a message
remote func send_message(gameid : int, chat : Array, message : Dictionary) -> void:
	message["Time"] = OS.get_unix_time()
	var GameData : Dictionary = Games[gameid]
	if GameData["Chats"].has(chat) == false:
		GameData["Chats"][chat] = []
	for player in chat:
		if GameData["OnlinePlayers"].has(player):
			for id in GameData["OnlinePlayers"][player]:
				rpc_id(id, "add_message", chat, message)
	GameData["Chats"][chat].append(message)

# Variable functions

# Checks if passed username is valid
func check_username(gameid : int, username : String) -> bool:
	var GameData : Dictionary = Games[gameid]
	var Player = GameData["Players"].get(username)
	return Player == null or Player["Occupied"] == false

# Checks if a username is present in a game
func get_username_in_game(gameid : int, username : String) -> bool:
	var GameData : Dictionary = Games[gameid]
	for player in GameData["Players"]:
		if GameData["Players"][player]["Username"] == username:
			return true
	return false

# Returns the username's player
func get_username_player(gameid : int, username : String) -> String:
	var GameData = Games[gameid]
	for player in GameData["Players"]:
		if GameData["Players"][player]["Username"] == username:
			return player
	# Failure case
	print("\"" + username + "\" is not in game " + str(gameid) + " player list")
	return ""

# Removes an id from all players variables
func disconnect_player(id : int) -> void:
	for gameid in Games:
		remove_player_id(gameid, id)

# Removes player id from passed game
remote func remove_player_id(gameid : int, id : int) -> void:
	var GameData : Dictionary = Games[gameid]
	var OnlinePlayers : Dictionary = GameData["OnlinePlayers"]
	for player in OnlinePlayers:
		for i in len(OnlinePlayers[player]):
			if OnlinePlayers[player][i] == id:
				OnlinePlayers[player].remove(i)
				if OnlinePlayers[player].empty():
					OnlinePlayers.erase(player)
				for onlineplayerid in get_online_player_ids(gameid):
					rpc_id(onlineplayerid, "remove_online_player", player)
			break

# Returns the values for game setup options
remote func get_game_setup_options(id : int) -> void:
	rpc_id(id, "set_game_setup_options", ConfigLoader.MaxPlayers, ConfigLoader.TickLengths)

# Returns the launch order for the passed submarine
func get_submarine_order(gameid : int, submarineid : int) -> Dictionary:
	var GameData : Dictionary = Games[gameid]
	for order in GameData["Orders"]:
		if order["Type"] == "Launch":
			if order["SubmarineId"] == submarineid:
				return order
	# Failure case
	return {}
